
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
python3 -m pip install --upgrade pip
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py loaddata db.json
python3 manage.py runserver 0.0.0.0:8000
deactivate
